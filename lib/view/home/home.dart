import 'package:flutter/material.dart';
import 'package:thomas_task/utils/app_strings.dart';
import 'package:thomas_task/utils/colors.dart';
import 'package:thomas_task/utils/constants.dart';
import 'package:thomas_task/utils/images.dart';
import 'package:thomas_task/utils/web_responsive.dart';
import 'package:thomas_task/view/home/tab_bar_section.dart';
import 'package:thomas_task/view/layout_widgets/web_app_bar.dart';
import 'package:thomas_task/widgets/app_text_theme.dart';
import 'package:thomas_task/widgets/custom_clippers.dart';
import 'package:thomas_task/widgets/custom_tab_bar_item.dart';
import 'package:thomas_task/widgets/rectangle_gradient_button.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  bool showButtonOnTopBar = false;
  late ScrollController tabScrollController, pageScrollController;

  @override
  void initState() {
    super.initState();
    tabScrollController = ScrollController();
    pageScrollController = ScrollController();
    pageScrollController.addListener(() {
      if (pageScrollController.offset > 325) {
        if (!showButtonOnTopBar) {
          setState(() {
            showButtonOnTopBar = true;
          });
        }
      } else {
        if (showButtonOnTopBar) {
          setState(() {
            showButtonOnTopBar = false;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            controller: pageScrollController,
            child: Column(
              children: [
                WebResponsive.isMobileView(context)
                    ? SizedBox(
                        height: 700,
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            ClipPath(
                              clipper: HomeTopSectionClipper(),
                              child: Container(
                                decoration: const BoxDecoration(
                                    gradient: appOnboardingJobGradients),
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 85,
                                    ),
                                    Text(
                                      AppString.deineJobWebsite,
                                      textAlign: TextAlign.center,
                                      style: AppTextTheme.bodyText42.copyWith(
                                        color: k2D3748,
                                      ),
                                    ),
                                    Expanded(
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            left: -40,
                                            right: -110,
                                            top: 0,
                                            bottom: 0,
                                            child: Image.asset(
                                              AppImages.homeAgreementImage,
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : ClipPath(
                        clipper: HomeTopSectionClipper(),
                        child: Container(
                          height: 550,
                          padding: const EdgeInsets.only(top: 85),
                          decoration: const BoxDecoration(
                              gradient: appOnboardingJobGradients),
                          child: Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 25.0),
                              child: ConstrainedBox(
                                constraints: const BoxConstraints(
                                  maxWidth: 800,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            AppString.deineJobWebsite,
                                            style: AppTextTheme.boldText42.copyWith(
                                              color: k2D3748,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 40,
                                          ),
                                          RectangleGradientButton(
                                            title: AppString.registration,
                                            onTap: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      flex: 7,
                                      child: Center(
                                        child: Container(
                                          height: 350,
                                          width: 390,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                            image: DecorationImage(
                                              image: AssetImage(
                                                AppImages.homeAgreementImage,
                                              ),
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                SizedBox(
                  height: WebResponsive.isMobileView(context) ? 27 : 35,
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 40,
                    width: 520,
                    child: ListView.builder(
                      controller: tabScrollController,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      scrollDirection: Axis.horizontal,
                      itemCount: tabBarItem.length,
                      itemBuilder: (context, index) => CustomTabBarItem(
                        index: index,
                        isSelected: index == currentIndex,
                        title: tabBarItem[index],
                        onTap: () {
                          var width = MediaQuery.of(context).size.width;
                          var offset = 520 - width;
                          if (width < 520) {
                            tabScrollController.animateTo(
                              index == 0
                                  ? 0
                                  : (index == 1 ? (offset / 2) : offset),
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease,
                            );
                          }
                          setState(() {
                            currentIndex = index;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: WebResponsive.isMobileView(context) ? 30 : 60,
                ),
                TabBarSection(
                  index: currentIndex,
                ),
                SizedBox(
                  height: WebResponsive.isMobileView(context) ? 130 : 0,
                ),
              ],
            ),
          ),
          WebAppBar(
            showButton: showButtonOnTopBar,
          ),
          if (WebResponsive.isMobileView(context))
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 90,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(12.0)),
                    boxShadow: [
                      BoxShadow(
                          color: k000033.withOpacity(0.25),
                          blurRadius: 3,
                          offset: const Offset(0, -1))
                    ]),
                alignment: Alignment.topCenter,
                padding:
                    const EdgeInsets.only(top: 24.0, left: 20.0, right: 20.0),
                child: RectangleGradientButton(
                  title: AppString.registration,
                  onTap: () {},
                ),
              ),
            ),
        ],
      ),
    );
  }
}
