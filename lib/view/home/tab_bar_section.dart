import 'package:dashed_line/dashed_line.dart';
import 'package:flutter/material.dart';
import 'package:thomas_task/utils/app_strings.dart';
import 'package:thomas_task/utils/colors.dart';
import 'package:thomas_task/utils/images.dart';
import 'package:thomas_task/utils/web_responsive.dart';
import 'package:thomas_task/widgets/app_text_theme.dart';
import 'package:thomas_task/widgets/custom_clippers.dart';

class TabBarSection extends StatelessWidget {
  final int index;

  const TabBarSection({required this.index, super.key});

  @override
  Widget build(BuildContext context) {
    return WebResponsive.isMobileView(context)
        ? _MobileContent(index: index)
        : _WebContent(index: index);
  }
}

class _MobileContent extends StatelessWidget {
  final int index;

  const _MobileContent({required this.index});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          index == 0
              ? AppString.dreiEinfacheSchritteZuDeinemNeuenJob
              : index == 1
                  ? AppString.dreiEinfacheSchritteZuDeinemNeuenMitarbeiter
                  : AppString
                      .dreiEinfacheSchritteZurVermittlungNeuerMitarbeiter,
          textAlign: TextAlign.center,
          style: AppTextTheme.bodyText21.copyWith(
            color: k4A5568,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        SizedBox(
          height: 225,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 40),
                  child: Image.asset(
                    AppImages.homeProfileDataImage,
                    height: 144,
                    width: 219,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '1. ',
                        style: AppTextTheme.numberText.copyWith(
                          fontSize: 130,
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            index == 0
                                ? AppString.erstellenDeinLebenslauf
                                : AppString.erstellenDeinUnternehmensprofil,
                            style: AppTextTheme.bodyText16.copyWith(
                              color: k718096,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 400,
          child: Stack(
            fit: StackFit.expand,
            children: [
              ClipPath(
                clipper: TabSection2Clipper(),
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: appOnboardingTabGradients,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 47.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '2. ',
                          style: AppTextTheme.numberText.copyWith(
                            fontSize: 130,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: Text(
                              index == 0
                                  ? AppString.erstellenDeinLebenslauf
                                  : index == 1
                                      ? AppString.erstellenEinJobinserat
                                      : AppString
                                          .erhalteVermittlungsangebotvonArbeitgeber,
                              style: AppTextTheme.bodyText16.copyWith(
                                color: k718096,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 80.0),
                      child: Image.asset(
                        index == 0
                            ? AppImages.homeTaskImage
                            : index == 1
                                ? AppImages.homeAboutMeImage
                                : AppImages.homeJobOfferImage,
                        height: 126,
                        width: 180,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        SizedBox(
          height: 360,
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                top: -30,
                left: -65,
                child: ClipOval(
                  clipper: CustomOvalClipper(),
                  child: Container(
                    height: 360,
                    width: 360,
                    color: kF7FAFC,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 56.0),
                    child: Row(
                      children: [
                        Text(
                          "3. ",
                          style: AppTextTheme.numberText.copyWith(
                            fontSize: 130,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            index == 0
                                ? AppString.mitNurEinemKlickBewerben
                                : index == 1
                                    ? AppString.wahleDeinenNeuenMitarbeiterAus
                                    : AppString
                                        .vermittlungNachProvisionOderStundenlohn,
                            style: AppTextTheme.bodyText16.copyWith(
                              color: k718096,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Image.asset(
                      index == 0
                          ? AppImages.homePersonalFileImage
                          : index == 1
                              ? AppImages.homeSwipeProfileImage
                              : AppImages.homeBusinessDealImage,
                      height: 210,
                      width: 281,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _WebContent extends StatelessWidget {
  final int index;

  const _WebContent({required this.index});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 420,
          left: 0,
          right: 0,
          child: ClipPath(
            clipper: TabSection2DesktopClipper(),
            child: Container(
              height: 300,
              decoration: const BoxDecoration(
                gradient: appOnboardingTabGradients,
              ),
            ),
          ),
        ),
        Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 800,
            ),
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Positioned(
                  left: 0,
                  top: 160,
                  child: Container(
                    height: 160,
                    width: 160,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF7FAFC),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  left: 25,
                  bottom: 70,
                  child: Container(
                    height: 220,
                    width: 230,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF7FAFC),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  left: 100,
                  top: 300,
                  child: SizedBox(
                    height: 270,
                    width: 375,
                    child: DashedLine(
                      path: arrowPathSecond(270.0, 375.0),
                      color: const Color(0xFF4A5568),
                      dashSpace: 2,
                      dashLength: 2,
                    ),
                  ),
                ),
                Positioned(
                  left: 135,
                  top: 650,
                  child: SizedBox(
                    height: 270,
                    width: 395,
                    child: DashedLine(
                      path: arrowPathFirst(270.0, 395.0),
                      color: const Color(0xFF4A5568),
                      dashSpace: 2,
                      dashLength: 2,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                          index == 0
                              ? AppString.dreiEinfacheSchritteZuDeinemNeuenJob
                              : index == 1
                                  ? AppString
                                      .dreiEinfacheSchritteZuDeinemNeuenMitarbeiter
                                  : AppString
                                      .dreiEinfacheSchritteZurVermittlungNeuerMitarbeiter,
                          textAlign: TextAlign.center,
                          style: AppTextTheme.bodyText36.copyWith(
                            color: k4A5568,
                          ),
                          ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 225,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 12),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      '1. ',
                                      style: AppTextTheme.numberText,
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? AppString
                                                  .erstellenDeinLebenslauf
                                              : AppString
                                                  .erstellenDeinUnternehmensprofil,
                                          style:AppTextTheme.bodyText16.copyWith(
                                            color: k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Image.asset(
                                AppImages.homeProfileDataImage,
                                fit: BoxFit.contain,
                                height: 170,
                              ),
                            ),
                            const Expanded(flex: 1, child: SizedBox())
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 400,
                        child: Row(
                          children: [
                            Expanded(
                              child: Image.asset(
                                index == 0
                                    ? AppImages.homeTaskImage
                                    : index == 1
                                        ? AppImages.homeAboutMeImage
                                        : AppImages.homeJobOfferImage,
                                fit: BoxFit.contain,
                                height: 170,
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 47.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      '2. ',
                                      style: AppTextTheme.numberText,
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? AppString
                                                  .erstellenDeinLebenslauf
                                              : index == 1
                                                  ? AppString
                                                      .erstellenEinJobinserat
                                                  : AppString
                                                      .erhalteVermittlungsangebotvonArbeitgeber,
                                          style: AppTextTheme.bodyText16.copyWith(
                                            color: k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 360,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 80.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      '3. ',
                                      style: AppTextTheme.numberText,
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? AppString
                                                  .mitNurEinemKlickBewerben
                                              : index == 1
                                                  ? AppString
                                                      .wahleDeinenNeuenMitarbeiterAus
                                                  : AppString
                                                      .vermittlungNachProvisionOderStundenlohn,
                                          style: AppTextTheme.bodyText16.copyWith(
                                            color: k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Image.asset(
                                index == 0
                                    ? AppImages.homePersonalFileImage
                                    : index == 1
                                        ? AppImages.homeSwipeProfileImage
                                        : AppImages.homeBusinessDealImage,
                                fit: BoxFit.contain,
                                height: 200,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
