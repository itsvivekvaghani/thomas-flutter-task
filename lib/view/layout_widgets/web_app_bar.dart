import 'package:flutter/material.dart';
import 'package:thomas_task/utils/app_strings.dart';
import 'package:thomas_task/utils/colors.dart';
import 'package:thomas_task/utils/web_responsive.dart';
import 'package:thomas_task/widgets/app_text_theme.dart';
import 'package:thomas_task/widgets/rectangle_button.dart';

class WebAppBar extends StatelessWidget {
  final bool showButton;
  const WebAppBar({
    super.key,
    required this.showButton,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          height: 5,
          decoration: const BoxDecoration(
            gradient: appBarGradients,
          ),
        ),
        Container(
          height: 62,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(12.0),
                  bottomRight: Radius.circular(12.0)),
              boxShadow: [
                BoxShadow(
                    color: k000029.withOpacity(0.2),
                    blurRadius: 6,
                    offset: const Offset(0, 3))
              ]),
          alignment: Alignment.centerRight,
          padding: const EdgeInsets.only(
            right: 17,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (showButton && WebResponsive.isWebView(context)) ...[
                Text(
                  "Jetzt Klicken",
                  style: AppTextTheme.bodyText16.copyWith(
                    fontWeight: FontWeight.w600,
                    letterSpacing: 0.84,
                    color:k4A5568,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                const RectangleButton(
                  title: AppString.registration,
                ),
                const SizedBox(
                  width: 20,
                ),
              ],
               Text(
                AppString.login,
                style: AppTextTheme.bodyText14.copyWith(
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.84,
                  color: k319795,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
