import 'package:flutter/material.dart';
import 'package:thomas_task/utils/colors.dart';
import 'package:thomas_task/widgets/app_text_theme.dart';

class RectangleButton extends StatelessWidget {
  const RectangleButton({super.key, required this.title, this.onTap});

  final String title;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: const Color(0xFFCBD5E0),
          ),
        ),
        alignment: Alignment.center,
        child: Text(
          title,
          style: AppTextTheme.recButtonText.copyWith(
            color: k319795,
          ),
        ),
      ),
    );
  }
}
