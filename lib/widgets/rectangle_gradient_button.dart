import 'package:flutter/material.dart';
import 'package:thomas_task/utils/colors.dart';
import 'package:thomas_task/widgets/app_text_theme.dart';

class RectangleGradientButton extends StatelessWidget {
  const RectangleGradientButton({super.key, required this.title, this.onTap});
  final String title;
  final void Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          gradient: rectangleButtonGradients,
          borderRadius: BorderRadius.circular(12),
        ),
        alignment: Alignment.center,
        child: Text(
          title,
          style: AppTextTheme.recButtonText,
        ),
      ),
    );
  }
}
