import 'package:flutter/material.dart';
import 'package:thomas_task/utils/colors.dart';

class AppTextTheme {
  static TextStyle recButtonText = const TextStyle(
    fontSize: 14,
    color: kE6FFFA,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.84,
  );

  static TextStyle bodyText42 = const TextStyle(
    fontSize: 42,
    letterSpacing: 1.26,
    height: 1.2,
    fontWeight: FontWeight.w500,
  );

  static TextStyle boldText42 = const TextStyle(
    fontSize: 42,
    letterSpacing: 1.26,
    height: 1.2,
    fontWeight: FontWeight.w700,
  );

  static TextStyle bodyText21 = const TextStyle(
    height: 1.2,
    fontSize: 21,
    fontWeight: FontWeight.w500,
  );

  static TextStyle bodyText36 = const TextStyle(
    height: 1.2,
    fontSize: 36,
    color: k4A5568,
    fontWeight: FontWeight.w500,
  );

  static TextStyle bodyText14 = const TextStyle(
    fontSize: 14,
    letterSpacing: 0.47,
    fontWeight: FontWeight.normal,
  );

  static TextStyle bodyText16 = const TextStyle(
    fontSize: 16,
    letterSpacing: 0.47,
    fontWeight: FontWeight.normal,
  );

  static TextStyle numberText = const TextStyle(
    fontSize: 100,
    height: 1,
    fontWeight: FontWeight.normal,
    color: k718096,
  );

}