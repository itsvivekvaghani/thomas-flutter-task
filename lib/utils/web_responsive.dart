import 'package:flutter/material.dart';

class WebResponsive extends StatelessWidget {
  final Widget webScreen;
  final Widget? mobileScreen;

  const WebResponsive({Key? key, required this.webScreen, this.mobileScreen})
      : super(key: key);

  static bool isMobileView(BuildContext context) {
    return MediaQuery.of(context).size.width <= 600;
  }

  static bool isWebView(BuildContext context) {
    return MediaQuery.of(context).size.width > 600;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 600) {
          return webScreen;
        } else {
          return mobileScreen ?? webScreen;
        }
      },
    );
  }
}
