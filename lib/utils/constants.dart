import 'package:thomas_task/utils/app_strings.dart';

final tabBarItem = [
  AppString.arbeitnehmer,
  AppString.arbeitgeber,
  AppString.temporarburo
];