class AppString {
  const AppString._();

  static const mainTitle = 'Deine Job Website';
  static const login = 'Login';
  static const deineJobWebsite = 'Deine Job\nwebsite';
  static const registration = 'Kostenlos Registrieren';
  static const arbeitnehmer = 'Arbeitnehmer';
  static const arbeitgeber = 'Arbeitgeber';
  static const temporarburo = 'Temporärbüro';
  static const dreiEinfacheSchritteZuDeinemNeuenJob = 'Drei einfache Schritte\nzu deinem neuen Job';
  static const erstellenDeinLebenslauf = 'Erstellen dein Lebenslauf';
  static const erstellenDeinUnternehmensprofil = 'Erstellen dein Unternehmensprofil';
  static const erstellenEinJobinserat = 'Erstellen ein Jobinserat';
  static const erhalteVermittlungsangebotvonArbeitgeber = 'Erhalte Vermittlungs- angebot von Arbeitgeber';
  static const mitNurEinemKlickBewerben = 'Mit nur einem Klick bewerben';
  static const dreiEinfacheSchritteZuDeinemNeuenMitarbeiter = 'Drei einfache Schritte\nzu deinem neuen Mitarbeiter';
  static const wahleDeinenNeuenMitarbeiterAus = 'Wähle deinen neuen Mitarbeiter aus';
  static const dreiEinfacheSchritteZurVermittlungNeuerMitarbeiter='Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter';
  static const vermittlungNachProvisionOderStundenlohn='Vermittlung nach Provision oder Stundenlohn';
}
