class AppImages {
  AppImages._();

  static const homeAgreementImage = 'assets/images/home_agreement_image.png';
  static const homeProfileDataImage = 'assets/images/home_profile_data_image.png';
  static const homeTaskImage = 'assets/images/home_task_image.png';
  static const homePersonalFileImage = 'assets/images/home_personal_file_image.png';
  static const homeAboutMeImage = 'assets/images/home_about_me_image.png';
  static const homeSwipeProfileImage = 'assets/images/home_swipe_profile_image.png';
  static const homeBusinessDealImage = 'assets/images/home_business_deal_image.png';
  static const homeJobOfferImage = 'assets/images/home_job_offer_image.png';

}